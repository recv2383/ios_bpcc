# ios_bpcc



## Setup for IOS
Please support: https://gitlab.com/magnolia1234/bypass-paywalls-chrome-clean


## Install software
* Go to App store and search for "Adlock" and install, direct link is: https://apps.apple.com/us/app/adlock-ad-blocker-privacy/id1506604517 and install

* Next search for Userscripts ,direct link is: https://apps.apple.com/us/app/userscripts/id1463298887 and install

## Configure Adlock

* Launch Adlock and run through the setup process, once running click on "Filters" under Advanced

* Next click "Add item" 

* Under Filter name enter something like "bypass" or any name you want

* Under Url enter: https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/bpc-paywall-filter.txt

* Next hit done and might take a bit to load

## Configure Userscripts

* Launch Userscripts and click the "Set Userscripts Directory"

* Click the "New folder" icon and create a new folder in the root structure called "User Scripts"

* Open a Safari and go to: https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/tree/main/userscript

*


## Enable Adlock

* Go to Settings -> Safari - > Extensions
